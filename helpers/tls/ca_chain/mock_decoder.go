// Code generated by mockery v2.14.0. DO NOT EDIT.

package ca_chain

import (
	x509 "crypto/x509"

	mock "github.com/stretchr/testify/mock"
)

// mockDecoder is an autogenerated mock type for the decoder type
type mockDecoder struct {
	mock.Mock
}

// Execute provides a mock function with given fields: data
func (_m *mockDecoder) Execute(data []byte) (*x509.Certificate, error) {
	ret := _m.Called(data)

	var r0 *x509.Certificate
	if rf, ok := ret.Get(0).(func([]byte) *x509.Certificate); ok {
		r0 = rf(data)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*x509.Certificate)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func([]byte) error); ok {
		r1 = rf(data)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTnewMockDecoder interface {
	mock.TestingT
	Cleanup(func())
}

// newMockDecoder creates a new instance of mockDecoder. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func newMockDecoder(t mockConstructorTestingTnewMockDecoder) *mockDecoder {
	mock := &mockDecoder{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
