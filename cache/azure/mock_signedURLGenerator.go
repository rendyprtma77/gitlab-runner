// Code generated by mockery v2.14.0. DO NOT EDIT.

package azure

import (
	url "net/url"

	mock "github.com/stretchr/testify/mock"
)

// mockSignedURLGenerator is an autogenerated mock type for the signedURLGenerator type
type mockSignedURLGenerator struct {
	mock.Mock
}

// Execute provides a mock function with given fields: name, options
func (_m *mockSignedURLGenerator) Execute(name string, options *signedURLOptions) (*url.URL, error) {
	ret := _m.Called(name, options)

	var r0 *url.URL
	if rf, ok := ret.Get(0).(func(string, *signedURLOptions) *url.URL); ok {
		r0 = rf(name, options)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*url.URL)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, *signedURLOptions) error); ok {
		r1 = rf(name, options)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTnewMockSignedURLGenerator interface {
	mock.TestingT
	Cleanup(func())
}

// newMockSignedURLGenerator creates a new instance of mockSignedURLGenerator. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func newMockSignedURLGenerator(t mockConstructorTestingTnewMockSignedURLGenerator) *mockSignedURLGenerator {
	mock := &mockSignedURLGenerator{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
